const { app, BrowserWindow, ipcMain, dialog } = require("electron");
const fs = require("fs");
const path = require("path");
const axios = require("axios");
const { Parser } = require("json2csv");
const dayjs = require("dayjs");
const _ = require("lodash");

function createWindow() {
  const win = new BrowserWindow({
    width: 550,
    height: 360,
    webPreferences: {
      nodeIntegration: true,
    },
    icon: __dirname + "/logo.png",
  });
  win.loadFile("index.html");

  win.setMenuBarVisibility(false);
}

app.whenReady().then(createWindow);

app.on("window-all-closed", () => {
  if (process.platform !== "darwin") {
    app.quit();
  }
});

app.on("activate", () => {
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow();
  }
});

ipcMain.on("selectFolder", (event, args) => {
  dialog
    .showOpenDialog({ properties: ["openDirectory"] })
    .then((result) => {
      let folderPath = result.filePaths[0];
      if (folderPath) {
        event.sender.send("folderSelected", { folder: folderPath });
      }
    })
    .catch((error) => console.error(error));
});

ipcMain.on("export", async (event, args) => {
  let options = {
    headers: {
      Authorization: args.token,
    },
  };

  let url = `https://api.dmarket.com/exchange/v1/history?offset=0&limit=${args.limit}`;

  try {
    const response = await axios.get(url, options);
    const exchangeHistoryItems = parseResponse(response.data);
    const csv = toCsv(exchangeHistoryItems);
    const filePath = getFilePath(args.folder);
    await writeToFile(filePath, csv);
    exportCompleted(event.sender, filePath);
  } catch (error) {
    exportFailed(event.sender, `Error: ${error}`);
  }
});

const parseResponse = (data) => {
  var items = data.objects.map((object) => {
    return {
      timestamp: new Date(object.updatedAt * 1000),
      type: object.type,
      action: object.action,
      subject: object.subject,
      amount: object.changes[0] ? object.changes[0].money.amount : "",
      balance: object.balance.amount,
      currency: object.balance.currency,
      status: object.status,
      changes: JSON.stringify(object.changes),
    };
  });
  return _.orderBy(items, ["timestamp", "balance"], ["desc", "desc"]);
};

const toCsv = (historyEntries) => {
  return new Parser().parse(historyEntries);
};

const getFilePath = (folder) => {
  let date = new Date();
  let timestamp = dayjs(date).format("YYYY-MM-DDTHHmmssZZ");
  return (filePath = path.join(folder, `dm-export-${timestamp}.csv`));
};

const writeToFile = (filePath, content) => {
  return new Promise((resolve, error) => {
    fs.writeFile(filePath, content, (err) => {
      if (err) {
        error(err);
      } else {
        resolve();
      }
    });
  });
};

const exportCompleted = (sender, file) => {
  sender.send("exportCompleted", { file });
};

const exportFailed = (sender, message) => {
  sender.send("exportFailed", { message });
};
